$(function () {
	var $adimg = $('.ad-img'), $adinfo = $('.adjust');
	$adinfo.css('width', $adimg.width() + 'px');

	$('#newimgad').on('change', function (e) {
		e.preventDefault();
		$('#loader-container').html('<i class="fa fa-refresh fa-spin loader"></i>');
		$('#uploadimg-form').ajaxForm({
			dataType : 'json',
			success : function (res) {
				if (res.status == 1) {
					$('.uploader').hide();
					$('.editor').fadeIn(300);

					var $dhref = $('#discardad').attr('href') + '/' + res.ad_id;
					$('#discardad').attr('href', $dhref);

					var $fact = $('#saveadvert-form').attr('action') + '/' + res.ad_id;
					$('#saveadvert-form').attr('action', $fact);

					$('#preview').html(res.message);

				}
			}
		}).submit();
	});

	$('#saveadvert-form').submit(function (e) {
		e.preventDefault();
		var $errors = 0;
		$(this).find(':input').each(function () {
			if ($(this).attr('name') != 'description' && $(this).val() == '') {
				$errors++;
				$(this).css('border-color', '#ea4940');
			}
		});
		if ($errors > 0) return false;
		$.ajax({
			url : $(this).attr('action'),
			type : 'POST',
			dataType : 'json',
			data : $(this).serializeObject(),
			success : function (r) {
				if (r.status == 1) {
					window.location.href=r.href;
				}
			}
		});
	});

	if ($('.share_link').length > 0) {
		var $link = $('.share_link').attr('data-sub');
		$.ajax({
			url : 'http://api.benjy.ch/create_url/jsonp/?url=' + $link,
			type : 'GET',
			dataType : 'jsonp',
			success : function (r) {
				if (r.code == 1) {
					$('.share_link span').html(r.link);
					$('.share_link').attr('href', r.link);
				}
			}
		});
	}

	$('.likes').click(function (e) {
		e.preventDefault();

		$adid = $(this).attr('data-ad-ref');
		$alike = $(this);

		$.ajax({
			url : 'http://thenewfragranceformen.com/ajax/like/' + $adid,
			type : 'GET',
			dataType : 'json',
			success : function (r) {
				if (r.status == 1) {
					$alike.find('i').addClass('liked');
					var $countspan = $alike.find('span'), $count = parseInt($countspan.text());
					$countspan.html($count + 1);
				}
			}
		});

	});

});







