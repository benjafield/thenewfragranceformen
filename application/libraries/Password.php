<?php

	class Password {

		public $_static, $_dynamic, $errors, $algorithm = 'sha256';
		var $r_hash_salt = 'a%+_%ln_r7+qi)0!p8*q';

		public function __construct () {
			$this->_static = hash('sha256', 'Wu2=TcEqrMH3ATAOE0b==2+8khap6p');
			$this->_dynamic = hash('sha256', 'i_change_per_user');
		}

		public function split_hash ($str, $blocks){
			$str = hash('sha256', $str);
			$block_len = ceil(strlen($str) / $blocks);
			$block_strs = str_split($str, $block_len);
			$glue = $this->_static . $this->_dynamic;

			return hash($this->algorithm, implode($glue, $block_strs));
		}

	}