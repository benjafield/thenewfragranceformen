<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['error_prefix'] = '<span class="error"><i class="fa fa-times-circle"></i>';
$config['error_suffix'] = '</span>';