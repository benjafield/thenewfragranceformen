<?= form_open('ajax/signup', 'id="sign-up-form"'); ?>
<p>
	<?= form_label('Name', 'name'); ?>
	<?= form_input('name', '', 'id="name" placeholder="Name"'); ?>
</p>
<p>
	<?= form_label('Username', 'username'); ?>
	<?= form_input('username', '', 'id="username" placeholder="thenewfragranceformen.com/USERNAME"'); ?>
</p>
<p>
	<?= form_label('Email Address', 'email'); ?>
	<input type="email" name="email" id="email" placeholder="me@something.com" autocomplete="off" autocapitalize="off" autocorrect="off" />
</p>
<p>
	<?= form_label('Password', 'password'); ?>
	<?= form_password('password', '', 'id="password" placeholder="Make it a strong one..."'); ?>
</p>
<p class="txtc">
	<?= form_submit('signup', 'Sign Up', 'class="button large"'); ?>
</p>
<?= form_close(); ?>