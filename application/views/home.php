<main>
	
	<? if (!$logged_in) : ?>
	<div class="full-band white shadow">
		<div class="notice">
			<h1 class="inline">The New Fragrance For Men</h1>
			<p class="inline">Parody advert platform. Make funny ads and share them.</p>
			<p class="inline ml">
				<a href="<?= site_url('signup'); ?>" class="button">Sign up and share</a>
			</p>
		</div>
	</div>
	<? else : ?>
		<? if (isset($stats) && $stats->ads == 0) : ?>
		<div class="full-band white shadow">
			<div class="notice">
				<p class="inline">You haven&apos;t created any adverts yet.</p>
				<p class="inline ml">
					<a href="<?= site_url('new'); ?>" class="button">Make one now</a>
				</p>
			</div>
		</div>
		<? endif; ?>
	<? endif; ?>

	<div class="content feed wrapper">
		
		<h1>Recent Adverts</h1>

		<div class="adfeed">
		<? if ( ! empty($results) ) : ?>
		<? foreach ($results as $item) : ?>

		<div class="aditem" data-href="http://<?= $item->subdomain; ?>.thenewfragranceformen.com">
			<div class="inner">
				<a class="image" style="background-image:url(<?= site_url($item->thumb); ?>);" href="http://<?= $item->subdomain; ?>.thenewfragranceformen.com"></a>
				<div class="addets">
					<a href="http://<?= $item->subdomain; ?>.thenewfragranceformen.com">
						<span class="title"><?= $item->title; ?></span>
						<span class="description"><?= substr($item->description, 0, 80); ?>...</span>
					</a>
				</div>
				<a class="creator" href="<?= site_url($item->username); ?>"><i class="fa fa-male"></i> <?= $item->username; ?></a>
				<a class="likes" data-ad-ref="<?= $item->id; ?>" title="Click to like this" href="#"><i class="fa fa-heart<?= ((in_array($item->id, $user_likes)) ? ' liked' : '' ); ?>"></i><span><?= $item->num_likes; ?></span></a>
			</div>
		</div>

		<? endforeach; ?>
		<? endif; ?>
		</div>

		<p class="pagination"><?= $links; ?></p>

	</div>

</main>