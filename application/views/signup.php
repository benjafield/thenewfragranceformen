<main>

	<div class="full-band white shadow">
		<div class="wrapper notice txtc">
			<h1>Sign Up</h1>
			<p>
				Start sharing parody adverts for free.
			</p>
		</div>
	</div>

	<div class="content wrapper form">
		<? if (isset($signup)) : ?>
		<span class="<?= $signup->status; ?>"><?= $signup->message; ?></span>
		<? endif; ?>
		<?= form_open('signup', 'id="sign-up-form"'); ?>
		<p>
			<?= form_label('Name', 'name'); ?>
			<?= form_input('name', set_value('name'), 'id="name" placeholder="Name"'); ?>
			<?= form_error('name'); ?>
		</p>
		<p>
			<?= form_label('Username', 'username'); ?>
			<?= form_input('username', set_value('username'), 'id="username" placeholder="thenewfragranceformen.com/USERNAME"'); ?>
			<?= form_error('username'); ?>
		</p>
		<p>
			<?= form_label('Email Address', 'email'); ?>
			<input type="email" name="email" id="email" placeholder="me@something.com" autocomplete="off" autocapitalize="off" autocorrect="off" value="<?= set_value('email'); ?>" />
			<?= form_error('email'); ?>
		</p>
		<p>
			<?= form_label('Password', 'password'); ?>
			<?= form_password('password', '', 'id="password" placeholder="Make it a strong one..."'); ?>
			<?= form_error('password'); ?>
		</p>
		<p class="txtc">
			<?= form_submit('signup', 'Sign Up', 'class="button large"'); ?>
		</p>
		<?= form_close(); ?>
	</div>

</main>