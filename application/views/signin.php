<main>

	<div class="full-band white shadow">
		<div class="wrapper notice txtc">
			<h1>Sign In</h1>
			<p>
				Not have an account? <a class="button ml" href="<?= site_url('signup'); ?>">Sign Up</a>
			</p>
		</div>
	</div>

	<div class="content wrapper form signin">
		
		<? if (isset($signin)) : ?>
		<span class="<?= $signin->class; ?> larger txtc"><i class="fa fa-times-circle"></i><?= $signin->message; ?></span>
		<? endif; ?>

		<?= form_open('signin', 'id="signin-form"'); ?>
		<p>
			<?= form_label('Username or Email Address', 'emailusername'); ?>
			<?= form_input('emailusername', set_value('emailusername'), 'id="emailusername" placeholder="Username or Email Address"'); ?>
			<?= form_error('emailusername'); ?>
		</p>
		<p>
			<?= form_label('Password', 'password'); ?>
			<?= form_password('password', '', 'id="password" placeholder="Password"'); ?>
			<?= form_error('password'); ?>
		</p>
		<p class="fp-link">
			<a href="<?= site_url('forgotpassword'); ?>">Lost Password?</a>
		</p>
		<p class="r-check txtr">
			<?= form_label('Remember Me?', 'remember', array('class' => 'checkbox')); ?>
			<?= form_checkbox('remember', '1', FALSE, 'id="remember"'); ?>
		</p>
		<p class="txtc">
			<?= form_submit('signin', 'Sign In', 'class="button large signin"'); ?>
		</p>
		<?= form_close(); ?>

	</div>

</main>