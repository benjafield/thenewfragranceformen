<main>

	<div class="adcontainer">
		<p class="txtc"><img src="<?= site_url($advert->content); ?>" alt="<?= $advert->title; ?> Full Advert" class="ad-img" /></p>

		<div class="info adjust">

			<div class="advert-info">
				<h1><?= $advert->title; ?></h1>
				<p>
					<?= $advert->description; ?>
				</p>
				<div class="social">
					<a class="share_link" data-sub="<?= site_url(); ?>" href="#">Link: <span></span></a>
				</div>
			</div>

		</div>
	</div>

</main>