<div class="user">
	<a href="<?= site_url($user->username); ?>">
		<span><?= $user->name; ?></span>
		<img src="<?= site_url('assets/' . $user->profile_img); ?>" title="<?= $user->name; ?>" alt="<?= $user->username; ?> Main Image" />
		<span><i class="fa fa-caret-down"></i></span>
	</a>
	<div class="usermenu">
		<nav>
			<a href="<?= site_url($user->username); ?>">My Profile</a>
			<a href="<?= site_url('new'); ?>">New Advert</a>
			<a href="<?= site_url('settings'); ?>">Settings</a>
			<a href="<?= site_url('signout'); ?>">Sign Out</a>
		</nav>
	</div>
</div>