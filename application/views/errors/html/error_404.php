<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta name="author" content="Charlie Benjafield, cbenjafield.com" />
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<title>Error 404 - The New Fragrance For Men</title>
	<link href="/assets/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link href="/assets/css/styles.css" rel="stylesheet" type="text/css" />
</head>
<body class="notfoundcontent">

	<header class="main">
		<a class="logo" href="/"><i class="fa fa-male"></i><span>The New Fragrance For Men</span></a>
		<div class="tools">
				<nav class="links">
					<a href="/signin">Sign In</a>
					<a href="/signup">Sign Up</a>
				</nav>
				<form action="/ajax/search" method="post">
					<div class="search">
						<input type="text" name="search" id="search" placeholder="Enter Search" />
						<button type="submit"><i class="fa fa-search"></i></button>
					</div>
				</form>
		</div>
	</header>

	<main>

		<div class="full-band white shadow">
			<div class="wrapper notice txtc">
				<h1>404: A Cyber-Space Odyssey</h1>
				<p>
					Looks like we couldn&apos;t find that page.
				</p>
			</div>
		</div>

		<div class="content wrapper">
			
		</div>

	</main>

</body>
</html>