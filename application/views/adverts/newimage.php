<main>

	<div class="full-band white shadow">
		<div class="notice">
			<h1 class="inline">Upload an Image</h1>
			<p class="inline">Upload a JPEG, PNG or GIF (and yes, it can be an animated one).</p>
		</div>
	</div>

	<div class="create new_image">
		<div class="uploader">
			<div class="wrapper txtc">
				<?= form_open_multipart('ajax/uploadimage', 'id="uploadimg-form"'); ?>
				<p>
					<label class="button large" for="newimgad"><i class="fa fa-picture-o"></i>Choose An Image</label>
					<input type="file" name="newimgad" id="newimgad" class="label-upload" />
				</p>
				<p id="loader-container" class="txtc">
					A preview of your advert will be shown once you have chosen an image.
				</p>
				<?= form_close(); ?>
			</div>
		</div>
		<div class="editor">
			<?= form_open('ajax/saveadvert', 'id="saveadvert-form"'); ?>
			<div class="prev" id="preview">
				<div class="nonajax-upload">
					<?= form_upload('advertupload', '', 'id="advertupload"'); ?>
				</div>
			</div>
			<div class="edit form">
				<h2>Tell us about your advert</h2>
				
				<p>
					<?= form_label('Title', 'title'); ?>
					<?= form_input('title', '', 'id="title" placeholder="Title"'); ?>
				</p>
				<p>
					<?= form_label('Subdomain (e.g. myadvert)', 'subdomain'); ?>
					<?= form_input('subdomain', '', 'id="subdomain" placeholder="SUBDOMAIN.thenewfragranceformen.com"'); ?>
				</p>
				<p>
					<?= form_label('Description', 'description'); ?>
					<textarea name="description" id="description" placeholder="Description"></textarea>
				</p>
				<p>
					<?= form_label('Privacy', 'public'); ?>
					<select name="public" id="public">
						<option value="1">Public - anyone can see it</option>
						<option value="0">Private - only people with the link</option>
					</select>
				</p>
				<p class="txtc">
					<a class="discard" id="discardad" href="<?= site_url('adverts/remove'); ?>">Discard this Advert</a>
					<?= form_hidden('type', '1'); ?>
					<?= form_submit('saveadvert', 'Save Advert', 'class="button"'); ?>
				</p>
			</div>
			<?= form_close(); ?>
		</div>
	</div>

</main>