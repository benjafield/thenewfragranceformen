<main>

	<div class="full-band white shadow">
		<div class="wrapper notice txtc">
			<h1 class="inline">Create a new Parody Advert</h1>
			<p class="inline">Follow the instructions below to create an advert.</p>
		</div>
	</div>

	<div class="content wizard">
		
		<div class="slide" data-href="1">
			
			<h2>What is your advert going to be?</h2>
			<a class="option" href="<?= site_url('new/image'); ?>"><i class="fa fa-picture-o"></i>Image</a>
			<a class="option" href="<?= site_url('new/web'); ?>"><i class="fa fa-code"></i>HTML &amp; CSS</a>
			<a class="option" href="<?= site_url('new/meme'); ?>"><i class="fa fa-smile-o"></i>Meme</a>

		</div>

	</div>

</main>