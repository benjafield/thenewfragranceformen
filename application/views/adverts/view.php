<main>

	<div class="wrapper txtc">
		<div class="ad-content">
			
			<div class="ad-info">
				<div class="addetails">
					<h3><?= $ad->title; ?></h3>
					<p>
						<?= $ad->description; ?>
					</p>
				</div>
				<p>
					<img src="<?= site_url($ad->content); ?>" title="<?= $ad->title; ?>" alt="<?= $ad->title; ?> Content" />
				</p>
			</div>

		</div>
	</div>

</main>