<main>

	<div class="profile wrapper">
		<div class="header">
			<img src="<?= site_url('assets/' . $profile->profile_picture); ?>" />
			<h1><a href="<?= site_url($profile->username); ?>"><?= $profile->name; ?></a></h1>
			<h2>
				<a href="<?= site_url($profile->username); ?>"><i class="fa fa-male"></i> <?= $profile->username; ?></a>
			</h2>
		</div>
		<div class="content">
			<h1>Adverts by <a href="<?= site_url($profile->username); ?>"><?= $profile->name; ?></a></h1>
		</div>
		<div class="adfeed">
			<? if ( ! empty($results) ) : ?>
			<? foreach ($results as $item) : ?>

			<div class="aditem" data-href="http://<?= $item->subdomain; ?>.thenewfragranceformen.com">
				<div class="inner">
					<a class="image" style="background-image:url(<?= site_url($item->thumb); ?>);" href="http://<?= $item->subdomain; ?>.thenewfragranceformen.com"></a>
					<div class="addets">
						<a href="http://<?= $item->subdomain; ?>.thenewfragranceformen.com">
							<span class="title"><?= $item->title; ?></span>
							<span class="description"><?= substr($item->description, 0, 80); ?>...</span>
						</a>
					</div>
					<a class="creator" href="<?= site_url($item->username); ?>"><i class="fa fa-male"></i> <?= $item->username; ?></a>
				</div>
			</div>

			<? endforeach; ?>
			<? else : ?>
			<p>
				<?= $profile->name; ?> has not uploaded anything yet.
			</p>
			<? endif; ?>
		</div>
	</div>

</main>