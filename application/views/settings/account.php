<main>
	
	<div class="full-band white shadow">
		<div class="notice">
			<h1>Change your account settings</h1>
		</div>
	</div>

	<div class="content wrapper form settings">
		
		<?= form_open('settings', 'id="account-settings-form"'); ?>
		<p>
			<?= form_label('Name', 'name'); ?>
			<?= form_input('name', $user->name, 'id="name" placeholder="Name"'); ?>
			<?= form_error('name'); ?>
		</p>
		<p>
			<?= form_label('Email Address', 'email'); ?>
			<input type="email" name="email" id="email" placeholder="me@something.com" autocomplete="off" autocapitalize="off" autocorrect="off" value="<?= $user->email; ?>" />
			<?= form_error('email'); ?>
		</p>
		<p>
			<?= form_label('How do you want your adverts posted?', 'public_default'); ?>
			<select name="public" id="public_default">
				<option value="1">Public - anyone can view your new adverts</option>
				<option value="0">Private - only you can see your new adverts</option>
			</select>
		</p>
		<hr />
		<h2>Change your password</h2>
		<p>
			<?= form_label('Current Password', 'current_password'); ?>
			<?= form_password('current_password', '', 'id="current_password" autocomplete="off" autocorrect="off"'); ?>
			<?= form_error('current_password'); ?>
		</p>
		<p>
			<?= form_label('New Password', 'new_password'); ?>
			<?= form_password('new_password', '', 'id="new_password" autocomplete="off" autocorrect="off"'); ?>
			<?= form_error('new_password'); ?>
		</p>
		<p>
			<?= form_label('Confirm Password', 'confirm_password'); ?>
			<?= form_password('confirm_password', '', 'id="confirm_password" autocomplete="off" autocorrect="off"'); ?>
			<?= form_error('confirm_password'); ?>
		</p>
		<p class="txtc">
			<?= form_submit('savesettings', 'Save These Settings', 'class="button large"'); ?>
		</p>
		<?= form_close(); ?>


	</div>

</main>