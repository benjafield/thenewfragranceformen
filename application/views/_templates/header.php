<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta name="author" content="Charlie Benjafield, cbenjafield.com" />
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<title><?= $title; ?> - The New Fragrance For Men</title>
	<?= link_tag('assets/css/font-awesome.min.css'); ?>
	<?= link_tag('assets/css/styles.css'); ?>
</head>
<body>

	<header class="<?= ((isset($hf_class) && $hf_class !== 'main') ? $hf_class : 'main'); ?>">
		<a class="logo" href="http://thenewfragranceformen.com"><i class="fa fa-male"></i><span>The New Fragrance For Men</span></a>
		<div class="tools">
				<? if ( ! $logged_in) : ?>
				<nav class="links">
					<a href="<?= site_url('signin'); ?>">Sign In</a>
					<a href="<?= site_url('signup'); ?>">Sign Up</a>
				</nav>
				<? else : ?>
				<? $this->load->view('modules/header_user'); ?>
				<? endif; ?>
				<?= form_open('ajax/search'); ?>
				<div class="search">
					<?= form_input('search', '', 'id="search" placeholder="Enter Search"'); ?>
					<button type="submit"><i class="fa fa-search"></i></button>
				</div>
				<?= form_close(); ?>
		</div>
	</header>