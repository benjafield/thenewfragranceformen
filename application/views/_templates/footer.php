	<footer class="<?= ((isset($hf_class) && $hf_class !== 'main') ? $hf_class : 'main'); ?>">
		<p>
			<span class="logo db"><i class="fa fa-male"></i>The New Fragrance For Men</span>
			<a href="http://cbenjafield.com" target="_blank">&copy; Charlie Benjafield, <?= date('Y'); ?></a>
		</p>
	</footer>

	<script type="text/javascript" src="<?= site_url('assets/js/ext.scripts.js'); ?>"></script>
	<script type="text/javascript" src="<?= site_url('assets/js/jquery.tnffm.js'); ?>"></script>

</body>
</html>