<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta name="author" content="Charlie Benjafield, cbenjafield.com" />
	<meta name="description" content="<?= $advert->description; ?> - thenewfragranceformen.com" />
	<meta name="keywords" content="" />
	<title><?= $title; ?></title>
	<?= link_tag('assets/css/font-awesome.min.css'); ?>
	<link rel="stylesheet" type="text/css" href="<?= site_url('assets/css/ad_styles.css'); ?>" />
	<style type="text/css">
		html, body { background-image:linear-gradient(#<?= $colours[1]; ?>, #<?= $colours[0]; ?>); }
	</style>
</head>
<body>

	<header class="main">
		<a class="logo" href="http://thenewfragranceformen.com"><i class="fa fa-male"></i><span>The New Fragrance For Men</span></a>
		<div class="tools">
				<? if ( ! $logged_in) : ?>
				<nav class="links">
					<a href="http://thenewfragranceformen.com/signin">Sign In</a>
					<a href="http://thenewfragranceformen.com/signup">Sign Up</a>
				</nav>
				<? else : ?>
				<? $this->load->view('modules/header_user'); ?>
				<? endif; ?>
				<?= form_open('ajax/search'); ?>
				<div class="search">
					<?= form_input('search', '', 'id="search" placeholder="Enter Search"'); ?>
					<button type="submit"><i class="fa fa-search"></i></button>
				</div>
				<?= form_close(); ?>
		</div>
	</header>