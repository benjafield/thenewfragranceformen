<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Advert_model extends CI_Model {

	var $ci, $user = '';

	public function __construct ()
	{
		parent::__construct();
		$this->ci =& get_instance();
	}

	public function create_ad ( $data = '' ) {

		if (empty($data)) return false;

		$insert = $this->ci->db->insert('adverts', $data);

		return $this->ci->db->insert_id();

	}

	public function update_ad ( $data = '', $where )
	{

		if (empty($data)) return false;

		$update = $this->ci->db->update('adverts', $data, $where);

		return true;

	}

	public function clean_ads ()
	{

		// Delete from adverts table where TEMP = 1 and SAVED = 0 and
		// the user id is equal to the logged in user.
		if ($this->ci->user_model->logged_in())
		{

			$user_id = $this->ci->session->userdata('tnffm_user_id');

			// First the info about the ad is needed -> to delete the img
			$where = array(
				'temp' => 1,
				'saved' => 0,
				'user_id' => $user_id
			);
			$ads = $this->ci->db->get_where('adverts', $where);

			if ($ads->num_rows() == 0)
			{
				log_message('DEBUG', 'There were no adverts to delete.');
				return false;
			}

			foreach ($ads->result() as $ad)
			{
				if ($ad->ad_type == 1)
				{
					$img_path = realpath($_SERVER['DOCUMENT_ROOT'] . '/' . $ad->content);
					if ($img_path)
					{
						unlink($img_path);
					}
					$thumb = realpath($_SERVER['DOCUMENT_ROOT'] . '/' . $ad->thumb);
					if ($thumb)
					{
						unlink($thumb);
					}
				}
			}

			// The delete query.
			$this->ci->db->delete('adverts', $where);
		}

		log_message('DEBUG', 'Unsaved adverts deleted.');
		return true;

	}

	public function get_ad ( $ad_id = '' )
	{
		if (empty($ad_id)) return false;

		$sql = "SELECT `adverts`.*, `users`.`username` FROM `adverts`, `users` WHERE `adverts`.`user_id` = `users`.`id` AND `adverts`.`id` = ?";
		$ad = $this->ci->db->query($sql, array('id' => $ad_id));
		if ($ad->num_rows() == 0) return false;
		return $ad->row();
	}

	public function count_ads ($user_id = '')
	{
		if (empty($user_id)) return $this->ci->db->count_all('adverts');

		$this->ci->db->where(array('user_id' => $user_id, 'public' => 1, 'saved' => 1));
		$this->ci->db->from('adverts');

		return $this->ci->db->count_all_results();

	}

	public function feed ( $limit = 0, $start = 0, $user_id = '' )
	{

		$data = array();

		$sql = "SELECT DISTINCT `adverts`.*, `users`.`username`, (SELECT COUNT(`likes`.`id`) FROM `likes` WHERE `likes`.`ad_id` = `adverts`.`id`) as `num_likes` FROM `adverts`, `users`, `likes` WHERE `adverts`.`user_id` = `users`.`id` AND `adverts`.`public` = 1" . ((!empty($user_id)) ? " AND `adverts`.`user_id` = ? " : '') . ' ORDER BY `created_at` DESC';

		if ($limit !== 0)
		{
			if ($start > 0) $start = ($start - 1) * $limit;
			$sql .= " LIMIT {$start},{$limit}";
		}

		if (!empty($user_id))
		{
			$ads = $this->ci->db->query($sql, array('id' => $user_id));
		}
		else
		{
			$ads = $this->ci->db->query($sql);
		}

		if ($ads->num_rows() == 0) return '';

		foreach ($ads->result() as $ad)
		{
			$data[] = $ad;
		}

		return $data;

	}

	public function get_ad_by_subdomain ( $subdomain = '' )
	{
		if (empty($subdomain)) return false;
		$sql = "SELECT `adverts`.*, `users`.`username` FROM `adverts`, `users` WHERE `adverts`.`user_id` = `users`.`id` AND `adverts`.`subdomain` = ?";
		$ad = $this->ci->db->query($sql, array('subdomain' => $subdomain));
		if ($ad->num_rows() == 0) return false;
		return $ad->row();
	}

	public function like ($user_id, $advert_id)
	{
		$chk = $this->ci->db->get_where('likes', array('user_id' => $user_id, 'ad_id' => $advert_id));
		if ($chk->num_rows() == 0 && $this->ci->user_model->logged_in()) {
			$this->ci->db->insert('likes', array('user_id' => $user_id, 'ad_id' => $advert_id));
			return 1;
		}
		return 0;
	}

}






