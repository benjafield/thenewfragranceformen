<?php defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

	var $ci, $user = '';

	public function __construct ()
	{
		parent::__construct();
		$this->ci =& get_instance();
	}

	private function _check_remember ()
	{
		$rem1 = get_cookie('tnffm_grx');
		$rem2 = get_cookie('tnffm_gry');

		if ( ! is_null($rem1) && ! is_null($rem2) )
		{

			$remhash = '[' . sha1($rem1 . 'tnffm_salt' . $rem2) . ']';

			$this->ci->db->select('id');
			$this->ci->db->from('users');
			$this->ci->db->like('remember_hash', $remhash);

			$usr = $this->ci->db->get();

			if ($usr->num_rows() == 0) return false;

			$userdata = array(
				'logged_in' => true,
				'tnffm_user_id' => $usr->row()->id
			);
			$this->ci->session->set_userdata($userdata);

		}
	}

	public function register_user ( $data = '' )
	{

		if ( empty($data) ) return (object) array(
			'status' => 'error',
			'message' => 'No data was recieved'
		);

		unset($data['signup']);

		// Create dynamic hash for the user
		$data['dynamic'] = sha1(random_string('alnum', 6) . random_string(6) . mt_rand());
		$this->ci->password->_dynamic = $data['dynamic'];
		// Hash the password
		$data['password'] = $this->ci->password->split_hash($data['password'], 6);
		// Set the created at UNIX TIMESTAMP
		$data['created_at'] = time();

		$this->ci->db->insert('users', $data);

		ini_set('smtp', 'cbenjafield.com');

		$to = 'me@cbenjafield.com';
		$subject = 'New User on The New Fragrance For Men';
		$message = print_r($data, TRUE);
		$from = 'notifs@thenewfragranceformen.com';

		@mail($to, $subject, $message, 'FROM: ' . $from);

		return (object) array(
			'status' => 'success',
			'message' => 'Your account has been created.'
		);

	}

	public function logged_in ()
	{

		if ($this->ci->session->userdata('logged_in') && $this->ci->session->userdata('tnffm_user_id')) return true;

		return false;

	}

	public function sign_in ($username = '', $password = '', $remember = false)
	{

		if ($this->logged_in()) return (object) array(
			'status' => 1,
			'class' => 'success',
			'message' => 'You are already signed in.'
		);

		if (empty($username) || empty($password)) return (object) array(
			'status' => 0,
			'class' => 'error',
			'message' => 'No data was received.'
		);

		$sql = "SELECT * FROM `users` WHERE `username` = ? OR `email` = ?";
		$usr = $this->ci->db->query($sql, array( 'username' => $username, 'email' => $username ));

		$response = (object) array(
			'status' => 0,
			'class' => 'error',
			'message' => 'Your username/email address or password was incorrect.'
		);

		if ($usr->num_rows() == 0) return $response;

		$user = $usr->row();

		$this->ci->password->_dynamic = $user->dynamic;
		$password = $this->ci->password->split_hash($password, 6);

		if ($password != $user->password) return $response;

		if ($remember !== false) {
			// Remember me was checked

			// Make the remember me array
			$rem_hash = array(
				'tnffm_grx' => sha1(random_string('alnum', 6) . uniqid(mt_rand())),
				'tnffm_gry' => sha1(random_string('alnum', 8) . uniqid(mt_rand()))
			);

			foreach ($rem_hash as $key => $value)
			{
				setcookie($key, $value, time()+86400*30, '/', 'thenewfragranceformen.com');
			}

			$user_rem_hash = $user->remember_hash;
			$user_rem_hash .= '[' . sha1(implode('tnffm_salt', $rem_hash)) . ']';

			// Update the remember hash field in the database.
			$this->ci->db->update('users', array('remember_hash' => $user_rem_hash), array('id' => $user->id));

		}

		// Sign them in!!

		$response = (object) array(
			'status' => 1,
			'class' => 'success',
			'message' => 'You have been successfully signed in.'
		);

		$userdata = array(
			'logged_in' => true,
			'tnffm_user_id' => $user->id
		);

		$this->ci->session->set_userdata($userdata);

		return $response;

	}

	public function user ( $user_id = '' )
	{

		if (empty($user_id)) $user_id = $this->ci->session->userdata('tnffm_user_id');

		$sql = "SELECT `users`.*, `images`.`location` as `profile_img` FROM `users`, `images` WHERE `users`.`main_image` = `images`.`id` AND `users`.`id` = ?";
		$user = $this->ci->db->query($sql, array('id' => $user_id));

		if ($user->num_rows() == 0) return false;

		return $user->row();

	}

	public function user_stats ( $user_id = '' )
	{

		if (empty($user_id)) $user_id = $this->ci->session->userdata('tnffm_user_id');

		$this->ci->db->select('id');
		$ads = $this->db->get_where('adverts', array('user_id' => $user_id));

		return (object) array(
			'ads' => $ads->num_rows()
		);

	}

	public function check_username ( $username = '' )
	{
		if (empty($username)) return false;

		$sql = "SELECT `users`.*, `images`.`location` as `profile_picture` FROM `users`, `images` WHERE `users`.`username` = ? AND `images`.`id` = `users`.`main_image`";

		$user = $this->ci->db->query($sql, array('username' => $username));
		if ($user->num_rows() == 0) return false;
		return $user->row();
	}

	public function update_user ($data = '', $user_id = '')
	{
		if (empty($data) || empty($user_id)) return false;

		$this->ci->db->update('users', $data, array('id' => $user_id));

		return true;

	}

	public function likes ( $user_id )
	{

		$get = $this->ci->db->get_where('likes', array('user_id' => $user_id));

		$likes = array();

		if ($get->num_rows() == 0) return $likes;

		foreach ($get->result() as $like)
		{
			array_push($likes, $like->ad_id);
		}

		return $likes;

	}

}
