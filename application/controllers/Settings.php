<?php defined('BASEPATH') OR exit('No direct script access allowed.');

class Settings extends CI_Controller {

	var $logged_in = false, $user = '';

	public function __construct ()
	{
		parent::__construct();

		if ($this->user_model->logged_in())
		{
			$this->logged_in = true;
			$this->data['logged_in'] = $this->logged_in;
			$this->user = $this->user_model->user();
			$this->data['user'] = $this->user;
		}

		$this->_check_login();

	}

	private function _check_login ()
	{
		if (!$this->logged_in) redirect(site_url());
	}

	public function index ()
	{

		$this->form_validation->set_rules('name', 'name', 'required|min_length[3]',
			array(
				'required' => 'You have not entered a %s.',
				'min_length' => 'Your %s must be longer than 3 letters.'
			));

		$this->form_validation->set_rules('email', 'email address', 'required|valid_email|trim', array(
				'required' => 'You have not provided an %s.',
				'valid_email' => 'You must supply a valid %s.'
			));

		if ($this->input->post('email') !== $this->user->email)
		{
			$this->form_validation->set_rules('email', 'email address', 'is_unique[users.email]', array('is_unique' => 'That %s is already taken.'));
		}

		$password = array(
			'current' => $this->input->post('current_password'),
			'new' => $this->input->post('new_password'),
			'confirm' => $this->input->post('confirm_password'),
			'dynamic' => $this->user->dynamic,
			'password' => $this->user->password
		);

		if (!empty($password['current']))
		{
			$this->form_validation->set_rules('current_password', 'current password', 'callback_password_check');
			$this->form_validation->set_rules('new_password', 'new password', 'required', array(
					'required' => 'You need to supply a %s.'
				));
			$this->form_validation->set_rules('confirm_password', 'confirm password', 'required|matches[new_password]', array(
					'required' => 'You need to confirm the password',
					'matches' => 'The new password and confirmation password do not match.'
				));
		}

		if ($this->form_validation->run() === TRUE)
		{

			$settings = array(
				'name' => $this->input->post('name'),
				'email' => $this->input->post('email'),
				'default_ads_public' => $this->input->post('public'),
				'updated_at' => time()
			);

			if (!empty($password['new_password']))
			{
				$this->password->_dynamic = $this->user->dynamic;
				$settings['password'] = $this->password->split_hash($this->input->post('password'), 6);
			}

			if($this->user_model->update_user($settings, $this->user->id))
			{
				redirect('settings');
			}

		}

		$this->_render('settings/account', 'Settings');

	}

	public function password_check ($password) {
		$this->password->_dynamic = $this->user->dynamic;
		$password = $this->password->split_hash($password, 6);
		if ($password == $this->user->password) {
			return TRUE;
		} else {
			$this->form_validation->set_message('password_check', 'The password you supplied was incorrect.');
			return FALSE;
		}

	}

	private function _render ($filename = '', $title = '') {

		$this->data['title'] = $title;

		$this->load->view('_templates/header', $this->data);
		$this->load->view($filename, $this->data);
		$this->load->view('_templates/footer', $this->data);

	}

}