<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	var $logged_in = false, $user = '';

	public function __construct ()
	{
		parent::__construct();

		$this->data['user_likes'] = array();

		if ($this->user_model->logged_in())
		{
			$this->logged_in = true;
			$this->user = $this->user_model->user();
			$this->data['user'] = $this->user;
			$this->data['user_likes'] = $this->user_model->likes($this->user->id);
		}

		$this->data['logged_in'] = $this->logged_in;

	}

	public function index ( $page = '' )
	{

		// Check to see if we are accessing through a subdomain!
		$curl = explode('.', $_SERVER['HTTP_HOST'], 2);

		// die('<pre>'.print_r($curl, TRUE).'</pre>');

		if (isset($curl[0]) && $curl[0] !== 'scapture' && $curl[0] !== 'thenewfragranceformen')
		{
			$subdomain = $curl[0];
			if (!$advert = $this->advert_model->get_ad_by_subdomain($subdomain)) show_404();
			$this->data['advert'] = $advert;

			$this->data['colours'] = colorPalette(site_url($advert->content), 10);

			return $this->_public_render('public/adverts/view', $advert->title);
		}

		// We are not in a subdomain.

		$this->advert_model->clean_ads();

		if ($this->logged_in) $this->data['stats'] = $this->user_model->user_stats();

		$this->load->library('pagination');

		$config = array(
			'base_url' => base_url(),
			'total_rows' => $this->advert_model->count_ads(),
			'per_page' => 6,
			'uri_segment' => 1,
			'use_page_numbers' => true,
			'display_pages' => false,
			'next_link' => '<i title="Next Page" class="fa fa-arrow-circle-right"></i>',
			'prev_link' => '<i title="Previous Page" class="fa fa-arrow-circle-left"></i>'
		);

		$this->pagination->initialize($config);

		$page = (($this->uri->segment(1)) ? $this->uri->segment(1) : 0);

		// $page = ($page - 1) * $config['per_page'];

		$this->data['results'] = $this->advert_model->feed($config['per_page'], $page);
		$this->data['links'] = $this->pagination->create_links();

		$this->_render('home', 'Parody Adverts');

	}

	public function signup ()
	{

		$this->form_validation->set_rules('name', 'name', 'required|min_length[3]', array(
				'required' => 'You have not provided your %s.',
				'min_length' => 'Your $s must be longer than 3 letters.'
			));
		$this->form_validation->set_rules('username', 'username', 'required|min_length[3]|is_unique[users.username]', array(
				'required' => 'You have not provided a %s.',
				'is_unique' => 'That %s is already taken.'
			));
		$this->form_validation->set_rules('email', 'email address', 'required|is_unique[users.email]|valid_email|trim', array(
				'required' => 'You have not provided an %s.',
				'is_unique' => 'That %s is already taken.',
				'valid_email' => 'You must supply a valid %s.'
			));
		$this->form_validation->set_rules('password', 'password', 'required|min_length[6]', array(
				'required' => 'You have not provided a %s.',
				'min_length' => 'Your %s must be at least 6 characters long.'
			));

		if ($this->form_validation->run() == TRUE)
    {
      $this->data['signup'] = $this->user_model->register_user($this->input->post());
      if ($this->data['signup']->status == 'success')
      {
      	$this->session->set_flashdata('signup_status', true);
      	redirect('signin');
      }
    }

		$this->_render('signup', 'Sign Up');

	}

	public function signin ()
	{

		if ($this->logged_in) redirect(site_url());

		$this->form_validation->set_rules('emailusername', 'email address or username', 'required', array(
				'required' => 'Please enter an email address or username to sign in.'
			));
		$this->form_validation->set_rules('password', 'password', 'required', array(
				'required' => 'You must enter a password to sign in.'
			));

		if($this->form_validation->run() == TRUE)
		{
			$rem = (($this->input->post('remember') && $this->input->post('remember') == 1) ? true : false);

			$this->data['signin'] = $this->user_model->sign_in($this->input->post('emailusername'), $this->input->post('password'), $rem);
			if ($this->data['signin']->status == 1) redirect(site_url());
		}

		$this->_render('signin', 'Sign In');

	}

	private function _render ($filename = '', $title = '')
	{

		$this->data['title'] = $title;

		$this->load->view('_templates/header', $this->data);
		$this->load->view($filename, $this->data);
		$this->load->view('_templates/footer', $this->data);

	}

	private function _public_render ($filename = '', $title = '')
	{
		$this->data['title'] = $title;

		$this->load->view('_templates/public_header', $this->data);
		$this->load->view($filename, $this->data);
		$this->load->view('_templates/public_footer', $this->data);
	}

	public function signout ()
	{

		$this->session->sess_destroy();
		redirect(site_url());

	}

}
