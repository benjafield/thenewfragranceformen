<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {

	var $logged_in = false, $user = '';

	public function __construct ()
	{
		parent::__construct();

		if ($this->user_model->logged_in())
		{
			$this->logged_in = true;
			$this->user = $this->user_model->user();
			$this->data['user'] = $this->user;
		}

		$this->data['logged_in'] = $this->logged_in;

	}

	public function index ()
	{

		$username = $this->uri->segment(1);
		if (!isset($username) || empty($username)) show_404();

		if (!$userdata = $this->user_model->check_username($username)) show_404();

		$this->data['profile'] = $userdata;

		$this->load->library('pagination');

		$config = array(
			'base_url' => base_url(),
			'total_rows' => $this->advert_model->count_ads($userdata->id),
			'per_page' => 6,
			'uri_segment' => 3
		);

		$this->pagination->initialize($config);

		$page = (($this->uri->segment(2)) ? $this->uri->segment(3) : 0);
		$this->data['results'] = $this->advert_model->feed($config['per_page'], $page, $userdata->id);
		$this->data['links'] = $this->pagination->create_links();

		$this->_render('profiles/user', $userdata->username);

	}

	private function _render ($filename = '', $title = '')
	{

		$this->data['title'] = $title;

		$this->load->view('_templates/header', $this->data);
		$this->load->view($filename, $this->data);
		$this->load->view('_templates/footer', $this->data);

	}

}