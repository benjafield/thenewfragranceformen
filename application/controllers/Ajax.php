<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends CI_Controller {

	var $logged_in = false, $user = '';

	public function __construct ()
	{
		parent::__construct();

		if ($this->user_model->logged_in())
		{
			$this->logged_in = true;
			$this->data['logged_in'] = $this->logged_in;
			$this->user = $this->user_model->user();
			$this->data['user'] = $this->user;
		}

	}

	private function _json ($array = array())
	{
		die(json_encode($array));
	}

	public function uploadimage ()
	{

		$json = array(
			'status' => 0
		);

		$json['message'] = '<span class="error txtc">The image failed to upload.</span>';

		// Error Checking
		$spath = 'assets/ads/temp/';
		$path = realpath($spath);
		if ( ! $path) $this->_json($json);
		if ( empty($_FILES['newimgad']) || $_FILES['newimgad']['size'] < 1 ) $this->_json($json);

		$file = $_FILES['newimgad'];
		$name = $file['name'];
		$temp = $file['tmp_name'];
		$ext = strtolower(pathinfo($name, PATHINFO_EXTENSION));
		$ftypes = array('jpg', 'jpeg', 'png', 'gif');

		$newname = random_string('alnum', 10) . '_' . random_string('alnum', 5) . '.' . $ext;

		$mimes = array(
			'jpg' => 'image/jpeg',
			'jpeg' => 'image/jpeg',
			'png' => 'image/png',
			'gif' => 'image/gif'
		);

		if ( ! in_array($ext, $ftypes) )
		{
			$json['message'] = '<span class="error txtc">That file type is not allowed.</span>';
			$this->_json($json);
		}

		list($width, $height) = getimagesize($temp);

		$config = array(
			'image_library' => 'gd2',
			'source_image' => $temp,
			'maintain_ratio' => true,
			'new_image' => $path . '/' . $newname
		);

		if ($width > 1000 || $height > 1000)
		{
			$config['width'] = 1000;
			$config['height'] = 1000;
		}

		$this->load->library('image_lib', $config);
		$this->image_lib->resize();

		$config['width'] = 300;
		$config['height'] = 400;
		$config['new_image'] = $path . '/thumbs/' . $newname;

		$this->image_lib->initialize($config);
		$this->image_lib->resize();

		$advert = array(
			'user_id'    => $this->user->id,
			'ad_type'    => 1,
			'content'    => $spath . $newname,
			'thumb'      => $spath . 'thumbs/' . $newname,
			'temp'       => 1,
			'saved'      => 0,
			'created_at' => time()
		);

		$ins = $this->advert_model->create_ad($advert);

		if (!$ins) 
		{
			$json['message'] = '<span class="error txtc">The image failed to upload.</span>';
			$this->_json($json);
		}

		$json = array(
			'status' => 1,
			'message' => '<img src="'.site_url($spath . $newname).'" title="Advert Preview" alt="Advert Preview" />',
			'ad_id' => $ins
		);

		$this->_json($json);

	}

	public function saveadvert ( $ad_id = '' )
	{

		$json = array(
			'status' => 1,
			'message' => 'Advert was successfully saved.', 
		);

		if (!$this->input->post())
		{
			$json['code'] = 0;
			$json['message'] = 'No data was received.';
			$this->_json($json);
		}

		$advert = array(
			'title' => $this->input->post('title'),
			'subdomain' => $this->input->post('subdomain'),
			'ad_type' => $this->input->post('type'),
			'public' => $this->input->post('public'),
			'temp' => 0,
			'saved' => 1,
		);

		if ($this->input->post('description')) $advert['description'] = $this->input->post('description');

		if (!empty($_FILES['advertupload']) && $_FILES['advertupload']['size'] > 0 && $advert['type'] == 1)
		{
			$spath = 'assets/ads/temp/';
			$path = realpath($spath);
			if ( ! $path) $this->_json($json);
			if ( empty($_FILES['advertupload']) || $_FILES['advertupload']['size'] < 1 ) $this->_json($json);

			$file = $_FILES['advertupload'];
			$name = $file['name'];
			$temp = $file['tmp_name'];
			$ext = strtolower(pathinfo($name, PATHINFO_EXTENSION));
			$ftypes = array('jpg', 'jpeg', 'png', 'gif');

			$newname = random_string('alnum', 10) . '_' . random_string('alnum', 5) . '.' . $ext;

			$mimes = array(
				'jpg' => 'image/jpeg',
				'jpeg' => 'image/jpeg',
				'png' => 'image/png',
				'gif' => 'image/gif'
			);

			if ( ! in_array($ext, $ftypes) )
			{
				$json['message'] = '<span class="error txtc">That file type is not allowed.</span>';
				$this->_json($json);
			}

			list($width, $height) = getimagesize($name);

			$config['image_library'] = 'gd2';
			$config['source_image'] = $temp;
			$config['maintain_ratio'] = TRUE;
			$config['new_image'] = $path . '/' . $newname;

			if ($width > 1000 || $height > 1000)
			{
				$config['width'] = 1000;
				$config['height'] = 1000;
			}

			$this->load->library('image_lib', $config);
			$this->image_lib->resize();

			$config['width'] = 300;
			$config['height'] = 400;
			$config['new_image'] = $path . '/thumbs/' . $newname;

			$this->image_lib->initialize($config);
			$this->image_lib->resize();

			$advert['content'] = $spath . $newname;
			$advert['thumb'] = $spath . 'thumbs/' . $newname;

		}

		if (empty($ad_id))
		{
			// We need to create the advert
			$advert['created_at'] = time();
			$this->advert_model->create_ad($advert);
		}
		else
		{
			$advert['updated_at'] = time();
			$this->advert_model->update_ad($advert, array('id' => $ad_id));
			$json['href'] = site_url('adverts/' . $ad_id);
		}

		$this->_json($json);

	}

	private function _check_login ()
	{
		if (!$this->logged_in) redirect(site_url());
	}

	public function search ()
	{

		

	}

	public function like ($ad_id = '')
	{
		if (empty($ad_id)) $this->_json(array('status' => 0));
		if (!$this->logged_in) $this->_json(array('status' => 3));
		$a = array(
			'status' => $this->advert_model->like($this->user->id, $ad_id)
		);
		$this->_json($a);
	}

}