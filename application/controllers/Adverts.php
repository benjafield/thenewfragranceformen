<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adverts extends CI_Controller {

	var $logged_in = false, $user = '';

	public function __construct ()
	{
		parent::__construct();

		if ($this->user_model->logged_in())
		{
			$this->logged_in = true;
			$this->data['logged_in'] = $this->logged_in;
			$this->user = $this->user_model->user();
			$this->data['user'] = $this->user;
		}

	}

	public function newad ()
	{

		$this->_check_login();
		$this->_render('adverts/new', 'Create an advert');

	}

	private function _render ($filename = '', $title = '') {

		$this->_check_login();

		$this->data['title'] = $title;

		$this->load->view('_templates/header', $this->data);
		$this->load->view($filename, $this->data);
		$this->load->view('_templates/footer', $this->data);

	}

	public function image ()
	{

		$this->_check_login();
		$this->_render('adverts/newimage', 'Upload an Image Advert');

	}

	public function web ()
	{

		$this->_check_login();

		$this->data['hf_class'] = 'web';

		$this->_render('adverts/newweb', 'Write a Web Advert');

	}

	public function meme ()
	{

		$this->_check_login();
		$this->data['hf_class'] = 'memegen';

		$this->_render('adverts/newmeme', 'Generate a Meme');

	}

	public function view ( $ad_id = '' )
	{

		$this->_check_login();

		$this->advert_model->clean_ads();

		if (empty($ad_id)) show_404();

		if( ! $advert = $this->advert_model->get_ad($ad_id)) show_404();

		$this->data['ad'] = $advert;

		$this->_render('adverts/view', $advert->title);

	}

	private function _check_login ()
	{
		if (!$this->logged_in) redirect(site_url());
	}

}
